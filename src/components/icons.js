import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import FavoriteOutlinedIcon from '@mui/icons-material/FavoriteOutlined';
import CheckBoxOutlineBlankOutlinedIcon from '@mui/icons-material/CheckBoxOutlineBlankOutlined';
import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';

export const FavIcon = ({liked, onClick}) => {
    return <>
        <div onClick={onClick}>
            {liked ? <FavoriteOutlinedIcon/> : <FavoriteBorderOutlinedIcon/>}
        </div>
    </>
}

export const CheckIcon = ({checked, onClick}) => {
    return <>
        <div onClick={onClick}>
            {checked ? <CheckBoxOutlinedIcon/> : <CheckBoxOutlineBlankOutlinedIcon/>}
        </div>
    </>
}

export const PlayIcon = ({playing, onClick}) => {
    return <>
        <div onClick={onClick}>
            {playing ? <img src="../../img/play.gif" style={{width: '36px'}} alt="play"/> : <PlayArrowIcon/> }
        </div>
    </>
}


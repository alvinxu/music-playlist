import {useSelector} from "react-redux";
import {selectMusicById, selectMusicIds} from "./homeSlice";
import {SongRow} from "./SongRow";

const AllList = () => {
    const musicIds = useSelector(selectMusicIds)

    let songRow = musicIds.map(musicId =>
        <SongRow
            key={musicId}
            musicId={musicId}
        />
    )

    return (
        <div className="listContainer">
            <div className="songList">
                <div className="list">
                    <div className="listRow title">All List</div>
                    <div className="listRow">
                        {songRow}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AllList

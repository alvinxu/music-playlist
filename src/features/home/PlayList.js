import {SongRow} from "./SongRow";
import {useSelector} from "react-redux";
import {selectAllMusics, selectMusicIds} from "./homeSlice";

const PlayList = () => {
    const musicIds = useSelector(selectMusicIds)
    const musics = useSelector(selectAllMusics)

    const playingMusicIds = musicIds.filter(musicId =>
        musics.find(music =>
            music.id === musicId).checked === true)

    let content = playingMusicIds.map(musicId =>
        <SongRow
            key={musicId}
            musicId={musicId}
        />
    )

    return <>
        <div className="listContainer">
            <div className="songList">
                <div className="list">
                    <div className="listRow title">Play List</div>
                    <div className="listRow">
                        {content}
                    </div>
                </div>
            </div>
        </div>
    </>
}
export default PlayList

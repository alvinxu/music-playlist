import './SongRow.scss'
import {FavIcon, CheckIcon, PlayIcon} from "../../components/icons";
import {useDispatch, useSelector} from "react-redux";
import {getPlayingId, toggleChecked, toggleLiked, togglePlay,setPlayingId} from "./homeSlice";
import {selectMusicById} from "./homeSlice";

export const SongRow = ({musicId}) => {

    const disPatch = useDispatch()

    const music = useSelector(state => selectMusicById(state, musicId))
    const playingId = useSelector(getPlayingId)


    const dispatchTogglePlay = async (musicId)=>{
        if(playingId && playingId !== musicId)
            await disPatch(togglePlay({musicId: playingId}))
        await disPatch(togglePlay({musicId: musicId}))
        await disPatch(setPlayingId({playingId: musicId}))
    }

    return <>
        <div className='songRow'>
            <div className="songRow_Index">
                {/*<div>{music.id}</div>*/}
                <div className="icon">
                    <CheckIcon
                        checked={music.checked}
                        onClick={() => disPatch(toggleChecked({musicId: music.id}))}
                    />
                </div>
            </div>
            <div className="songRow_Image">
                <img src={music.cover} width='48px' alt=""/>
            </div>
            <div className="songRow_Content">
                <div className="songRow_Content_Title">
                    <div className="songRow_Content_Title_Name">{music.title}</div>
                    <div className="songRow_Content_Title_Lyrics">{music.artist}</div>
                </div>
            </div>
            <div className="songRow_Length">{music.length}</div>
            <div className="songRow_Button">
                <div className='icon'>
                    <FavIcon
                        liked={music.liked}
                        onClick={() => disPatch(toggleLiked({musicId: music.id}))}
                    />
                </div>
                <PlayIcon
                    playing={music.playing}
                    onClick={()=>dispatchTogglePlay(music.id)}
                />
            </div>
        </div>
    </>
}


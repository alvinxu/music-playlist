import {useSelector} from "react-redux";
import {selectAllMusics, selectMusicIds} from "./homeSlice";
import {SongRow} from "./SongRow";

const FavList = () => {
    const musicIds = useSelector(selectMusicIds)
    const musics = useSelector(selectAllMusics)

    const favMusicIds = musicIds.filter(musicId =>
        musics.find(music =>
            music.id === musicId).liked === true)

    let content = favMusicIds.map(musicId =>
        <SongRow
            key={musicId}
            musicId={musicId}
        />
    )

    return <>
        <div className="listContainer">
            <div className="songList">
                <div className="list">
                    <div className="listRow title">Favorite List</div>
                    <div className="listRow">
                        {content}
                    </div>
                </div>
            </div>
        </div>
    </>
}
export default FavList

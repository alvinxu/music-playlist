import AllList from "./AllList";
import FavList from "./FavList";
import PlayList from "./PlayList";

import './Home.scss'

const Home = () => {
    return <>
        <section className='section'>
            <div className='container'>
                <AllList/>
                <FavList/>
                <PlayList/>
            </div>
        </section>
    </>

}
export default Home

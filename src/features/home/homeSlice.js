import {
    createAsyncThunk,
    createEntityAdapter,
    createSlice
} from "@reduxjs/toolkit";
import axios from "axios";
import {API_URL} from "../../helper";

//create adapter for data normalization
const musicAdapter = createEntityAdapter({
    sortComparer: (a, b) => a.id - b.id
})

//get initial state from adapter
const initialState = musicAdapter.getInitialState({
    playingId: null,
    status: 'idle', //'idle' | 'loading' | 'succeeded' | 'failed'
    error: null,
})

export const fetchMusics = createAsyncThunk('musics/fetchMusics', async () => {
    try {
        const response = await axios.get(API_URL)
        return response.data.data
    } catch (err) {
        console.log('Error in fetching music:', err)
    }
})

//slice creation
const homeSlice = createSlice({
    name: 'musics',
    initialState,
    reducers: {
        toggleChecked(state, action) {
            const {musicId} = action.payload
            const existingMusic = state.entities[musicId]
            if (existingMusic)
                existingMusic.checked = !existingMusic.checked
        },

        toggleLiked(state, action) {
            const {musicId} = action.payload
            const existingMusic = state.entities[musicId]
            if (existingMusic)
                existingMusic.liked = !existingMusic.liked
        },

        togglePlay(state, action) {
            const {musicId} = action.payload
            const existingMusic = state.entities[musicId]
            if (existingMusic)
                existingMusic.playing = !existingMusic.playing
        },

        setPlayingId(state,action){
            const {playingId} = action.payload
            state.playingId = playingId
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchMusics.fulfilled, (state, action) => {
                state.status = 'succeeded'
                //Adding liked,checked,and playing
                const loadedMusics = action.payload.map(music => {
                    music.checked = false
                    music.liked = false
                    music.playing = false
                    return music
                })
                musicAdapter.upsertMany(state, loadedMusics)
            })
    }
})

export const {
    selectAll: selectAllMusics,
    selectById: selectMusicById,
    selectIds: selectMusicIds
// Pass in a selector that returns the home slice of state
} = musicAdapter.getSelectors(state => state.musics)

export const getPlayingId = (state) => state.musics.playingId

export const {
    toggleChecked,
    toggleLiked,
    togglePlay,
    setPlayingId,
} = homeSlice.actions

export default homeSlice.reducer
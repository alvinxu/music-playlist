import {configureStore} from "@reduxjs/toolkit";
import musicReducer from '../features/home/homeSlice'

export const store = configureStore({
    reducer:{
        musics: musicReducer,
    }
})